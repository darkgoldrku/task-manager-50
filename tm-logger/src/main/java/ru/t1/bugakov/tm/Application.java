package ru.t1.bugakov.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.component.Bootstrap;

import javax.jms.JMSException;

public final class Application {

    public static void main(String[] args) throws JMSException {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
