package ru.t1.bugakov.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.service.LoggerService;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.IOException;

public final class EntityListener implements MessageListener {

    @NotNull
    private final LoggerService loggerService;

    public EntityListener(@NotNull final LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    public void onMessage(@Nullable final Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @Nullable final String text;
        try {
            text = textMessage.getText();
        } catch (@NotNull final JMSException e) {
            throw new RuntimeException(e);
        }
        try {
            loggerService.log(text);
        } catch (@NotNull final IOException e) {
            throw new RuntimeException(e);
        }
    }

}
