package ru.t1.bugakov.tm.api.repository.model;

import ru.t1.bugakov.tm.model.Session;

public interface ISessionRepository extends IAbstractUserOwnedRepository<Session> {

}
