package ru.t1.bugakov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.repository.model.ISessionRepository;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.model.ISessionService;
import ru.t1.bugakov.tm.model.Session;
import ru.t1.bugakov.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}
