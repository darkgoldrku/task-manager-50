package ru.t1.bugakov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.bugakov.tm.api.service.IConnectionService;
import ru.t1.bugakov.tm.api.service.dto.IProjectDTOService;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.bugakov.tm.exception.field.*;
import ru.t1.bugakov.tm.repository.dto.ProjectDTORepository;

import javax.persistence.EntityManager;

public final class ProjectDTOService extends AbstractUserOwnedDTOService<ProjectDTO, IProjectDTORepository> implements IProjectDTOService {

    public ProjectDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected IProjectDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDTORepository(entityManager);
    }

    @NotNull
    @Override
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

}
