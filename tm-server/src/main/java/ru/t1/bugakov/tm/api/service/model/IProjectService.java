package ru.t1.bugakov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Project;

public interface IProjectService extends IAbstractUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws Exception;

    @NotNull
    Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description);

    @NotNull
    Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status);

}
