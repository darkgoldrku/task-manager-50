package ru.t1.bugakov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.model.IAbstractUserOwnedRepository;
import ru.t1.bugakov.tm.model.AbstractUserOwnedModel;
import ru.t1.bugakov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IAbstractUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUser(entityManager.find(User.class, userId));
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void removeById(@NotNull String id) {
        @Nullable final M entity = findById(id);
        if (entity == null) return;
        remove(entity);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findById(userId, id) != null;
    }

    @Override
    public void clear(@NotNull String userId) {
        @Nullable final List<M> models = findAll(userId);
        if (models == null) return;
        for (@NotNull final M model : models) {
            remove(model);
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @Nullable final M model = findById(userId, id);
        if (model == null) return;
        remove(model);
    }

    @Nullable
    @Override
    public abstract List<M> findAll(@NotNull String userId);

    @Nullable
    @Override
    public abstract List<M> findAllWithSort(@NotNull String userId, @Nullable String sortField);

    @Nullable
    @Override
    public abstract M findById(@NotNull String userId, @NotNull String id);

    @Override
    public abstract int getSize(@NotNull String userId);

}


