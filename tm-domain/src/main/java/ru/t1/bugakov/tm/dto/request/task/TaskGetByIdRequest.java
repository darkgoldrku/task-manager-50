package ru.t1.bugakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskGetByIdRequest(@Nullable String id) {
        this.id = id;
    }

    public TaskGetByIdRequest(@Nullable String token, @Nullable String id) {
        super(token);
        this.id = id;
    }

}
