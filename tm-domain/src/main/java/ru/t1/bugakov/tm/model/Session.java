package ru.t1.bugakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConstants.TABLE_SESSION)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractUserOwnedModel {

    @Nullable
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

}
