package ru.t1.bugakov.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email is already exist.");
    }

}