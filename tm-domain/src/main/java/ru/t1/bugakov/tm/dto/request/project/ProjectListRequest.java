package ru.t1.bugakov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;
import ru.t1.bugakov.tm.enumerated.SortType;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private SortType projectSort;

    public ProjectListRequest(@Nullable SortType projectSort) {
        this.projectSort = projectSort;
    }

    public ProjectListRequest(@Nullable String token, @Nullable SortType projectSort) {
        super(token);
        this.projectSort = projectSort;
    }

}
